# Session Objectives

At the end of the session, the students are expected to:

- quickly create a web page design by using pre-built UI components from Bootstrap.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s11)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1wkhdUllk3oss-KLwmHxtIgnUa0ktfAkUHd6CKu9qQk8)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [User Interface Elements (Usability.gov)](https://www.usability.gov/how-to-and-tools/methods/user-interface-elements.html)
- [Bootstrap 4.6 Components](https://getbootstrap.com/docs/4.6/components/alerts/)

# Lesson Proper

## User Interface

A user interface is a feature that allows a user to interact with a given device or application. In web development, UI elements include but are not limited to the following:

- Search fields
- Dropdowns
- Date pickers
- Popups

## UI Components in Bootstrap

Using Bootstrap, we can quickly create these UI elements with a few lines of code. Bootstrap has an extensive documentation of components that can be created including pre-built customization options.

# Code Discussion

## Folder and File Preparation

Create a folder named **s11**, a folder named **discussion** inside the **s11** folder, then a file named **index.html** inside the **discussion** folder.

## Preliminary Code

Add the following initial code to the **index.html** file:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Bootstrap Components</title>
    </head>
    <body>
        <div class="container-fluid">
            <h1>Bootstrap Components</h1>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```

## Code Sample

Add the codes into **index.html** add discuss each section of the code (denoted by headings).

```html
<!-- Navbar -->
<nav class="navbar navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Navbar</a>
</nav>
```

Put the navbar before the div.container

```html
<h3>Buttons</h3>
<button type="button" class="btn btn-primary">Primary</button>
<button type="button" class="btn btn-secondary">Secondary</button>
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-danger">Danger</button>
<button type="button" class="btn btn-warning">Warning</button>
<button type="button" class="btn btn-info">Info</button>
<button type="button" class="btn btn-dark">Dark</button>
```

```html
<h3>Button Outlines</h3>
<button type="button" class="btn btn-outline-primary">Primary</button>
<button type="button" class="btn btn-outline-secondary">Secondary</button>
<button type="button" class="btn btn-outline-success">Success</button>
<button type="button" class="btn btn-outline-danger">Danger</button>
<button type="button" class="btn btn-outline-warning">Warning</button>
<button type="button" class="btn btn-outline-info">Info</button>
<button type="button" class="btn btn-outline-dark">Dark</button>
```

```html
<h3>Form Controls</h3>
<form>
    <div class="form-group">
        <label for="txt-email">Email address</label>
        <input type="email" class="form-control" id="txt-email" placeholder="name@example.com">
    </div>
    <div class="form-group">
        <label for="txt-select">Example select</label>
        <select class="form-control" id="txt-select">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
        </select>
    </div>
    <div class="form-group">
        <label for="txt-select-multiple">Example multiple select</label>
        <select multiple class="form-control" id="txt-select-multiple">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
        </select>
    </div>
    <div class="form-group">
        <label for="txt-textarea">Example textarea</label>
        <textarea class="form-control" id="txt-textarea" rows="3"></textarea>
    </div>
</form>
```

Additionally, you can demonstrate the following components based on the [documentation](https://getbootstrap.com/docs/4.6/components):

- [Alert](https://getbootstrap.com/docs/4.6/components/alerts/)
- [Card](https://getbootstrap.com/docs/4.6/components/card/)
- [Modal](https://getbootstrap.com/docs/4.6/components/modal/)

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

## Instructions

- Create a Contact section.
- Create a row & one column with a form that will accept the following fields:
    - Full Name (text input)
    - Email Address (email input)
    - Message (textarea)
- Add a submit button to the form.
- Put the row's contents to the center of the screen.
- Give 5 columns when the form is in medium-sized screens and up; 12 columns when the form is in small-sized screens.

## Expected Output

![readme-images/Untitled.png](readme-images/Untitled.png)

Sample output in a large screen.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Sample output in a small screen (Pixel 2 XL).

## Solution

The solution may vary from student to student, but the code may look like this:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Activity: Bootstrap Introduction and Simple Styles</title>
    </head>
    <body>
        <div class="p-4 container-fluid">
            <div class="text-center mb-4">
                <img class="rounded-circle mb-3" width="200" src="https://i.guim.co.uk/img/media/3656ae6ea2209d4561caf04fa9f172a519908ca3/0_28_2318_1391/master/2318.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=afcc355f47876bc495cdc3c902639bae"/>
                <h2>Ada Lovelace</h2>
                <h4>Full-Stack Web Developer</h4>
            </div>
            <p class="text-justify">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <h3 class="text-center">Projects</h3>
            <div class="row mt-3 mb-4">
                <div class="col-sm-12 col-md-6 order-2 order-md-1">
                    <h5 class="text-center pt-3">Web API (Ecommerce)</h5>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-sm-12 col-md-6 order-1 order-md-2">
                    <div class="text-center">
                        <img class="img-fluid" src="https://www.cloudways.com/blog/wp-content/uploads/Rest-API-introduction.jpg"/>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-sm-12 col-md-6 order-2 order-md-2">
                    <h5 class="text-center pt-3">React Frontend (Ecommerce)</h5>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-sm-12 col-md-6 order-1 order-md-1">
                    <div class="text-center">
                        <img class="img-fluid" src="https://d6vdma9166ldh.cloudfront.net/media/images/1455b5c8-4887-43a8-8214-de77543414c9.jpg"/>
                    </div>
                </div>
            </div>
            <h3 class="text-center">Tools</h3>
            <div class="d-flex flex-wrap justify-content-center mb-4">
                <img width="80" src="images/logo-html5.png"/>
                <img width="80" src="images/logo-css3.png"/>
                <img width="80" src="images/logo-javascript.png"/>
                <img width="80" src="images/logo-mongodb.png"/>
                <img width="80" src="images/logo-nodejs.png"/>
                <img width="80" src="images/logo-expressjs.png"/>
                <img width="80" src="images/logo-react.png"/>
                <img width="80" src="images/logo-heroku.png"/>
                <img width="80" src="images/logo-git.png"/>
                <img width="80" src="images/logo-sublime-text-3.png"/>
                <img width="80" src="images/logo-postman.png"/>
                <img width="80" src="images/logo-gitlab-ci-cd.png"/>
            </div>
            <h3 class="text-center">Contact</h3>
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-5">
                    <form>
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success btn-block">Send</button>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```